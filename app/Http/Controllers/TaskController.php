<?php

namespace App\Http\Controllers;

use DB;
use App\Task;
use Illuminate\Http\Request;


class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index')->with('tasks', $tasks);
    }

    public function showTask($id)
    {
        $task = Task::find($id);
        return view('tasks.show')->with(compact('task'));
    }
    public function addTask()
    {
        $task = new Task();
        $task->body = 'Learn Laravel';
        $task->save();
        return 'Ok!';
    }
}
