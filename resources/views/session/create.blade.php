@extends('layout')

@section('content')
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <h2>Sing in:</h2>
            <form method="post" action="/login">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <input type="submit" class="btn btn-primary" value="Sing in">
            </form>
            @include('posts.errors')
        </div>
    </div>
@stop