@extends('layout')

@section('content')
    <div class="col-sm-8">
        <h1>Registration</h1>
        <form action="/register" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">name:</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">email:</label>
                <input type="text" name="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">password:</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="password_confirmation">password conformation:</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Register</button>
        </form>
        @include('posts.errors')
    </div>
@stop