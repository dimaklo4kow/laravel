<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">General</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="/posts">Posts</a>
                </li>
                <li>
                    <a href="/posts/create">Post create</a>
                </li>



                @if(Auth::check())
                    <li class="navbar-link pull-right">
                        <a href="">{{Auth::user()->name}}</a>
                    </li>
                    <li class="navbar-link pull-right">
                        <a href="/logout">Logout</a>
                    </li>
                    @else
                    <li class="navbar-link pull-right">
                        <a href="/login">Login</a>
                    </li>
                    <li class="navbar-link pull-right">
                        <a href="/register">Register</a>
                    </li>
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>