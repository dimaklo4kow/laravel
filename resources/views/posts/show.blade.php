@extends('layout')

@section('content')
    <div class="col-md-4">
        <h2>{{$post->title}}</h2>
       @if(isset($post->picture))
            <img src="{{'/app/posts/'.$post->id.'/'.$post->picture}}" alt="">
           @endif
        <p>{{$post->body}}</p>
    </div>
    <div class="col-md-12 comments">
        <h3>Comments:</h3>
        <ul class="list-group">
            @foreach($post->comments AS $comment)
                <li class="list-group-item">
                    <strong>
                        {{$comment->created_at->diffForHumans()}}
                    </strong>
                    {{$comment->body}}
                </li>
            @endforeach
        </ul>
    </div>

    <div class="col-md-12">
        <div class="card-block">
            <form action="/posts/{{$post->alias}}/comments" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <textarea name="body" id="" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add comment</button>
                </div>
            </form>
            @include('posts.errors')
        </div>
    </div>
@stop
