@extends('layout')

@section('content')
    <div class="col-xs-6 col-lg-offset-3">
        <form method="POST" action="/posts" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">Заголовок</label>
                <input name="title" type="text" class="form-control" id="title" placeholder="Заголовок">
            </div>
            <div class="form-group">
                <label for="alias">Alias</label>
                <input name="alias" type="text" class="form-control" id="alias" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Статья</label>
                <textarea name="body" class="form-control" id="body" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="post_file">Выберете файл</label>
                <input type="file" name="post_file" id="post_file" class="form-control">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>


    @include('posts.errors')
        </form>
    </div>
@endsection
